# create the python image
FROM python:3

# set environment variable of pythonunbuffered to 1 so that python 
# output goes to the container (logs and such) without being buffered
ENV PYTHONUNBUFFERED 1

# make a code directory and set it to working directory 
RUN mkdir /code
WORKDIR /code

# copy requirements 
COPY requirements.txt /code/

# use pip to install requirements
RUN pip install -r requirements.txt

# copy the code base into the container
COPY .  /code/
