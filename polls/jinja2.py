from django.templatetags.static import static
from django.urls import reverse

from jinja2 import Environment

def environment(**options):
    env = Environment(**options)
    env.globals.update({
        'static': static,
        'url': reverse,
    })
    env.filters['jpluralize'] = jinja_pluralize
    return env


def jinja_pluralize(int):
    if int > 7:
        return "s"
    else:
        return ""

