# This content taken from 
# https://docs.djangoproject.com/en/3.1/intro/tutorial02/
from django.contrib import admin

from .models import Question

admin.site.register(Question)
