# this is from 
# https://docs.djangoproject.com/en/3.1/intro/tutorial04/
from django.shortcuts import render, get_object_or_404 
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.views import generic

from .models import Choice, Question

# import time used to check race condition behavior by voting several times
# within a given interval (enter delay in votes method > else statement below
# using time.sleep(15) and then vote in several windows within 15 seconds
# to create the race condition) - uncomment the import and the time.sleep()
# import time


class IndexView(generic.ListView):
    template_name = 'jinja2/index.html.jinja2'
    context_object_name = 'green_question_list'

    def get_queryset(self):
        """Return the last five published questions."""
        return Question.objects.order_by('-pub_date')[:5]


class DetailView(generic.DetailView):
    model = Question
    template_name = 'jinja2/detail.html.jinja2'


class ResultsView(generic.DetailView):
    model = Question
    template_name = 'jinja2/results.html.jinja2'


def jdetails(request, question_id):
    question = get_object_or_404(Question, pk=question_id)
    return render(request, 'jinja2/detail.html.jinja2', {'question': question,})
    

def vote(request, question_id):
    question = get_object_or_404(Question, pk=question_id)
    try:
        selected_choice = question.choice_set.get(pk=request.POST['choice'])
    except (KeyError, Choice.DoesNotExist):
        # Redisplay the question voting form.
        return render(request, 'polls/detail.html', {
            'question': question,
            'error_message': "You didn't select a choice.",
        })
    else:
        # time.sleep(15)  # this allows a human to create race condition
        selected_choice.votes += 1
        selected_choice.save()
        # Always return an HttpResponseRedirect after successfully dealing 
        # with POST data. This prevents data from being posted twice if a
        # user hits the Back button.
        return HttpResponseRedirect(reverse('polls:results', args=(question_id,)))
